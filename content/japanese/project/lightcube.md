---
title: LightCube
description: ''
image: images/portfolio/lc.png
bg_image: images/featue-bg.jpg
category: ゲーム制作
information:
- label: 名前
  info: LightCube
- label: 内容
  info: Game Creation
- label: ツール
  info: Unity
- label: 完了日
  info: 2020年6月
- label: スキル
  info: C#
- label: 製作者
  info: LightCube Team

---
## プロジェクト概要

このプロジェクトは、中学校の学習指導要領に記載されている「双方向通信」「プログラムの構造を支える要素」を含んだプログラミングゲームです。

> 処理の流れを図などに表し試行等 を通じて解決策を具体化する力などの育成や，順次，分岐，反復といったプログ ラムの構造を支える要素等の理解

> また，制作するコンテンツのプログラ ムに対して「ネットワークの利用」及び「双方向性」の規定を追加している。

文部科学省 中学校学習指導要領 平成29年 技術・家庭編 より

ゲーム内容は、車を混雑なく管理する信号をフローチャートで作るというものです。これに安全性を考慮してローカルネットワークで使用できるようにし、生徒個人またはグループで考え結論を導き出せるようになっています。

また、NiftyクラウドやPhotonを使ったユーザー管理式のオンライン版を製作する予定です。